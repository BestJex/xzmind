package xuanzi.xzmind.core;

/**
 * 文本存储读取接口
 * @author 彭立铭
 *
 */
public interface StoreSource {
	
	public static interface Result{
		void result(String result);
	} 
	
	boolean isAutoSave();
	
	public void read(Object path,Result res) ;
	
	public void save(Object path,String content,Result res) ;

	public String getName(Object path);

}
